using UnityEngine;
using System.Collections;

public class SoundButton : MonoBehaviour {

    int soundID;

    public int SoundID { set { soundID = value; } }

    Manager man;

    void Start()
    {
        man = FindObjectOfType<Manager>();
    }

	public void Interact()
    {
        man.PlaySound(soundID);
    }
}
