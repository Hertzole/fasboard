using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Manager : MonoBehaviour {

    [System.Serializable]
    public class AudioClass
    {
        public string audioName;
        public AudioClip audioClip;
        public AudioSource source;
        public string groupName;
    }

    public AudioClass[] audioClips;
    public string[] groups;

    public bool soundOverlapping = true;

    public Transform contentHolder;
    public Button uiButtonPrefab;
    public GameObject groupPrefab;
    public CanvasGroup loadingGroup;
    public CanvasGroup scrollRect;
    public Slider loadingBar;
    public Slider clipBar;
    public Text loadingText;
    public Text loadingHeader;
    public Text clipText;
    public Text clipTimeText;
    public Slider volumeSlider;
    public Button settingsButton;
    AudioSource[] allSources;

    public Toggle soundOverlappingToggle;

    int currentSoundID = 0;

    AudioClass currentAudio { get { return audioClips[currentSoundID]; } }

    void Start()
    {
        
        StartCoroutine(Setup());
        volumeSlider.value = PlayerPrefs.GetFloat("Volume", 1);
    }

    void Update()
    {
        if (audioClips[currentSoundID].source)
        {
            if (audioClips[currentSoundID].source.isPlaying)
            {
                clipBar.value = audioClips[currentSoundID].source.time;
                clipText.text = "Playing: " + currentAudio.audioName;
                clipTimeText.text = currentAudio.source.time.ToString("0") + " / " + currentAudio.audioClip.length.ToString("0");
            }
            else
            {
                clipText.text = "Currently not playing";
                clipTimeText.text = "0 / 0";
            }
        }
    }

    IEnumerator Setup()
    {
        DisableCanvasGroup(contentHolder.GetComponent<CanvasGroup>());
        DisableCanvasGroup(scrollRect);
        EnableCanvasGroup(loadingGroup);
        loadingBar.minValue = 0;
        settingsButton.interactable = false;

        loadingBar.maxValue = groups.Length;
        loadingHeader.text = "Loading Groups";
        for (int i = 0; i < groups.Length; i++)
        {
            loadingBar.value = i;
            GameObject newGroup = Instantiate(groupPrefab);
            newGroup.name = groups[i];
            newGroup.transform.SetParent(contentHolder);
            newGroup.transform.FindChild("GroupBox/GroupText").GetComponent<Text>().text = groups[i];
            loadingText.text = groups[i];
            yield return new WaitForSeconds(0.05f);
        }

        if (!soundOverlapping)
        {
            GameObject newAudioSource = new GameObject();
            newAudioSource.name = "Audio";
            newAudioSource.AddComponent<AudioSource>();
            newAudioSource.GetComponent<AudioSource>().playOnAwake = false;

            for (int i = 0; i < audioClips.Length; i++)
            {
                audioClips[i].source = newAudioSource.GetComponent<AudioSource>();
            }
        }

        loadingBar.maxValue = audioClips.Length;
        loadingHeader.text = "Loading Sounds";
        for (int i = 0; i < audioClips.Length; i++)
        {
            loadingBar.value = i;
            loadingText.text = audioClips[i].audioName;
            Button newButton = Instantiate(uiButtonPrefab);
            Transform groupParent = GameObject.Find(audioClips[i].groupName).transform;
            newButton.transform.SetParent(groupParent.FindChild("Content").transform);
            newButton.transform.FindChild("Text").GetComponent<Text>().text = audioClips[i].audioName;
            newButton.GetComponent<SoundButton>().SoundID = i;
            yield return new WaitForSeconds(0.01f);
        }

        DisableCanvasGroup(loadingGroup);
        EnableCanvasGroup(contentHolder.GetComponent<CanvasGroup>());
        EnableCanvasGroup(scrollRect);
        settingsButton.interactable = true;
    }

	public void PlaySound(int soundID)
    {
        currentSoundID = soundID;

        if (!audioClips[soundID].source)
        {
            GameObject newAudioSource = new GameObject();
            newAudioSource.name = audioClips[soundID].audioName;
            newAudioSource.AddComponent<AudioSource>();
            newAudioSource.GetComponent<AudioSource>().playOnAwake = false;
            audioClips[soundID].source = newAudioSource.GetComponent<AudioSource>();
        }

        audioClips[soundID].source.clip = audioClips[soundID].audioClip;
        audioClips[soundID].source.Play();

        clipBar.maxValue = audioClips[soundID].audioClip.length;
        clipBar.value = 0;
        clipText.text = "Playing: " + audioClips[soundID].audioName;
    }

    public void UpdateVolume(float newVolume)
    {
        AudioListener.volume = newVolume;
        PlayerPrefs.SetFloat("Volume", newVolume);
    }

    public void SetAudioTime(float newTime)
    {
        if (currentAudio.source)
        {
            if (currentAudio.source.isPlaying)
            {
                currentAudio.source.time = newTime;
            }
        }
    }

    public void StopAllSounds()
    {
        allSources = FindObjectsOfType<AudioSource>();

        foreach (AudioSource source in allSources)
        {
            source.Stop();
        }

        clipBar.value = clipBar.maxValue;
    }

    public void DisableCanvasGroup(CanvasGroup group)
    {
        group.alpha = 0;
        group.interactable = false;
        group.blocksRaycasts = false;
    }

    public void EnableCanvasGroup(CanvasGroup group)
    {
        group.alpha = 1;
        group.interactable = true;
        group.blocksRaycasts = true;
    }

    public void ApplySettings()
    {

    }
}
