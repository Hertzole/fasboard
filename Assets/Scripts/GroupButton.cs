using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GroupButton : MonoBehaviour {

    public Image expandIcon;
    public Sprite arrowUp;
    public Sprite arrowDown;

    public GameObject content;

    bool expanded = true;

	public void Interact()
    {
        expanded = !expanded;

        expandIcon.sprite = expanded ? arrowDown : arrowUp;

        foreach(Transform child in content.transform)
        {
            child.gameObject.SetActive(expanded);
        }
    }
}
